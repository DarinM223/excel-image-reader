import javafx.scene.layout.BorderPane
import javafx.stage.FileChooser
import tornadofx.View

class MainOptions : View() {
    override val root: BorderPane by fxml()

    fun newFile() {
        replaceWith<NewFile>()
    }

    fun openFile() {
        val chooser = FileChooser()
        chooser.title = "Open file"
        val file = chooser.showOpenDialog(this.currentWindow)
        file?.let { file ->
            val (settings, slides) = loadSlides(file)
            val openFileView = OpenFile(settings!!, slides)
            replaceWith(openFileView)
        }
    }

    override fun onDock() {
        super.onDock()
        currentWindow?.sizeToScene()
    }
}