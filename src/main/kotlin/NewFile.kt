import javafx.scene.control.Label
import javafx.scene.control.TextField
import javafx.scene.layout.BorderPane
import javafx.stage.DirectoryChooser
import javafx.stage.FileChooser
import tornadofx.View
import tornadofx.isInt
import java.io.File

class NewFile : View() {
    val settingsFile = "settings.txt"

    override val root: BorderPane by fxml()
    private val errorLabel: Label by fxid()

    private val excelFileLabel: Label by fxid()
    private val imageDirectoryLabel: Label by fxid()
    private val saveFileLabel: Label by fxid()

    private val startRowField: TextField by fxid()
    private val idColumnField: TextField by fxid()
    private val textColumnField: TextField by fxid()

    private var excelFile: File? = null
    private var imagesDirectory: File? = null
    private var saveFile: File? = null

    init {
        Settings.open(settingsFile)?.let { settings -> loadSettings(settings) }
    }

    private fun loadSettings(settings: Settings) {
        setExcelFile(File(settings.excelFilePath))
        setImagesDirectory(File(settings.imagesDirectoryPath))
        setSaveFile(File(settings.saveFilePath))
        startRowField.text = settings.startRow.toString()
        idColumnField.text = settings.idColumn.toString()
        textColumnField.text = settings.textColumn.toString()
    }

    private fun settingsFromParams(): Settings {
        return Settings(
                excelFilePath = excelFile?.absolutePath!!,
                imagesDirectoryPath = imagesDirectory?.absolutePath!!,
                saveFilePath = saveFile?.absolutePath!!,
                startRow = startRowField.text.toIntOrNull()!!,
                idColumn = idColumnField.text.toIntOrNull()!!,
                textColumn = textColumnField.text.toIntOrNull()!!
        )
    }

    private fun setExcelFile(newExcelFile: File?) {
        excelFile = newExcelFile ?: excelFile
        excelFileLabel.text = excelFile?.absolutePath ?: excelFileLabel.text
    }

    private fun setImagesDirectory(newImagesDirectory: File?) {
        imagesDirectory = newImagesDirectory ?: imagesDirectory
        imageDirectoryLabel.text = imagesDirectory?.absolutePath ?: imageDirectoryLabel.text
    }

    private fun setSaveFile(newSaveFile: File?) {
        saveFile = newSaveFile ?: saveFile
        saveFileLabel.text = saveFile?.absolutePath ?: saveFileLabel.text
    }

    fun openExcelFile() {
        val chooser = FileChooser()
        chooser.title = "Open excel file"
        setExcelFile(chooser.showOpenDialog(this.currentWindow))
    }

    fun openImagesDirectory() {
        val chooser = DirectoryChooser()
        chooser.title = "Open images directory"
        setImagesDirectory(chooser.showDialog(this.currentWindow))
    }

    fun createSaveFile() {
        val chooser = FileChooser()
        chooser.title = "Make new save file"
        setSaveFile(chooser.showSaveDialog(this.currentWindow))
    }

    fun constraints(): List<Constraint> {
        return listOf(
                FileConstraint(
                        excelFile,
                        "Please specify the path to the excel file",
                        "Excel file does not exist"
                ),
                FileConstraint(
                        imagesDirectory,
                        "Please specify the path to the directory of images",
                        "Image directory does not exist"
                ),
                NewFileConstraint(
                        saveFile,
                        "Please specify the path to the save file"
                ),
                PositiveIntConstraint(
                        startRowField.text,
                        "Starting row input must be an integer greater than or equal to zero"
                ),
                PositiveIntConstraint(
                        idColumnField.text,
                        "ID column input must be an integer greater than or equal to zero"
                ),
                PositiveIntConstraint(
                        textColumnField.text,
                        "Text column input must be an integer greater than or equal to zero"
                )
        )
    }

    fun isValid(): Boolean {
        val err = checkConstraints(this.constraints())
        errorLabel.text = err ?: ""
        return err == null
    }

    fun createFile() {
        if (!this.isValid()) {
            return
        }

        val settings = settingsFromParams()
        settings.save(settingsFile)

        saveSlides(
                excelFile = excelFile ?: return,
                saveFile = saveFile ?: return,
                settings = settings
        )

        goBack()
    }

    fun goBack() {
        replaceWith<MainOptions>()
    }
}
