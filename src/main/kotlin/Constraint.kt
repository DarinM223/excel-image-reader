import tornadofx.isInt
import java.io.File

interface Constraint {
    fun check(): String?
}

class FileConstraint(
        private val file: File?,
        private val noPathErr: String,
        private val noExistErr: String
) : Constraint {
    override fun check(): String? {
        if (file == null) {
            return noPathErr
        }
        if (!file.exists()) {
            return noExistErr
        }
        return null
    }
}

class NewFileConstraint(private val file: File?, private val noPathErr: String) : Constraint {
    override fun check(): String? {
        return if (file == null) noPathErr else null
    }
}

class PositiveIntConstraint(private val s: String, private val err: String) : Constraint {
    override fun check(): String? {
        return if (!s.isInt() || s.toInt() < 0) err else null
    }
}

fun checkConstraints(constraints: List<Constraint>): String? {
    for (constraint in constraints) {
        val err = constraint.check()
        if (err != null) {
            return err
        }
    }
    return null
}