import javafx.application.Application
import javafx.scene.text.TextAlignment
import tornadofx.*

class Styles : Stylesheet() {
    init {
        textArea {
            fontSize = 20.px
            text {
                textAlignment = TextAlignment.CENTER
            }
        }
    }
}

class ExcelImageApp : App(MainOptions::class, Styles::class) {
    init {
        reloadStylesheetsOnFocus()
    }
}

fun main(args: Array<String>) {
    Application.launch(ExcelImageApp::class.java, *args)
}