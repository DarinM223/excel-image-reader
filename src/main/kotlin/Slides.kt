import org.apache.poi.ss.usermodel.CellType
import org.apache.poi.xssf.usermodel.XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import java.io.*

data class Slide(val text: String, val imagePath: String, val prefix: String)

private fun parseLine(line: String, imageDirectory: File): Slide? {
    val firstSpace = line.indexOfFirst { ch -> ch == ' ' }
    if (firstSpace == -1) {
        return null
    }

    val prefix = line.substring(0, firstSpace)
    val path = File(imageDirectory, prefix + ".jpg").absolutePath
    val text = line.substring(firstSpace + 1)

    return Slide(text, path, prefix)
}

fun loadSlides(file: File): Pair<Settings?, List<Slide>> {
    val reader = file.bufferedReader()
    val settingsLine = reader.readLine()
    val settings = Settings.fromString(settingsLine)
    val dir = File(settings?.imagesDirectoryPath)

    val slides = mutableListOf<Slide>()
    for (line in reader.readLines()) {
        val slide = parseLine(line, dir)
        if (slide != null) {
            slides.add(slide)
        }
    }

    return Pair(settings, slides)
}

private tailrec fun readExcelFile(
        sheet: XSSFSheet,
        idCol: Int,
        textCol: Int,
        i: Int,
        build: List<Pair<String, String>>
): List<Pair<String, String>> {
    val row = sheet.getRow(i) ?: return build
    val col1 = row.getCell(idCol) ?: return build
    if (col1.cellTypeEnum == CellType.BLANK) {
        return build
    }

    val col2 = row.getCell(textCol)
    val build = build + Pair(col1.stringCellValue ?: "", col2?.stringCellValue ?: "")
    return readExcelFile(sheet, idCol, textCol, i + 1, build)
}

fun saveSlides(
        excelFile: File,
        saveFile: File,
        settings: Settings
) {
    val fileStream = FileInputStream(excelFile)
    val workbook = XSSFWorkbook(fileStream)
    val sheet = workbook.getSheetAt(0)

    // Read excel file and parse rows as long as the id column has a value.
    val idsAndTexts = readExcelFile(
            sheet,
            settings.idColumn,
            settings.textColumn,
            settings.startRow,
            listOf()
    )

    // Write to text file
    saveFile.writeText("")
    saveFile.appendText(settings.toString())
    saveFile.appendText("\n")
    for ((id, text) in idsAndTexts) {
        saveFile.appendText("$id $text\n")
    }
}

