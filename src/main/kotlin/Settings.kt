import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import java.io.File

data class Settings(
    val excelFilePath: String,
    val imagesDirectoryPath: String,
    val saveFilePath: String,
    val startRow: Int,
    val idColumn: Int,
    val textColumn: Int
) {
    companion object {
        fun fromString(s: String): Settings? {
            val mapper = jacksonObjectMapper()
            return mapper.readValue(s)
        }

        fun open(filePath: String): Settings? {
            val file = File(filePath)
            if (!file.exists()) return null

            val mapper = jacksonObjectMapper()
            return mapper.readValue(file)
        }
    }

    fun save(filePath: String) {
        val file = File(filePath)

        val mapper = jacksonObjectMapper()
        mapper.writeValue(file, this)
    }

    override fun toString(): String {
        val mapper = jacksonObjectMapper()
        return mapper.writeValueAsString(this)
    }
}