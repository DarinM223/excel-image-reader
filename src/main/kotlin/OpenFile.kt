import javafx.scene.control.Label
import javafx.scene.control.TextArea
import javafx.scene.control.TextInputDialog
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import javafx.scene.input.KeyCode
import javafx.scene.input.KeyEvent
import javafx.scene.layout.BorderPane
import tornadofx.View
import java.io.File

class OpenFile(val settings: Settings, slideList: List<Slide>) : View() {
    override val root: BorderPane by fxml()
    private val pageLabel: Label by fxid()
    private val imageView: ImageView by fxid("image")
    private val textArea: TextArea by fxid("text")

    var slides = ArrayList(slideList)
    var currIndex = 0

    init {
        currentStage?.width = 1000.0
        currentStage?.height = 800.0
        loadIndex(currIndex)
        currentStage?.addEventFilter(KeyEvent.KEY_PRESSED) {
            when (it.code) {
                KeyCode.LEFT -> prevPage()
                KeyCode.RIGHT -> nextPage()
            }
        }
    }

    private fun loadSlide(slide: Slide) {
        val file = File(slide.imagePath)
        val image = Image(file.toURI().toString())
        imageView.image = image
        textArea.text = slide.text
        pageLabel.text = "Page: " + slide.prefix
    }

    private fun loadIndex(index: Int) {
        slides.getOrNull(index)?.let { slide ->
            currIndex = index
            loadSlide(slide)
        }
    }

    fun jumpPage() {
        val dialog = TextInputDialog()
        dialog.showAndWait().ifPresent {
            loadIndex(slides.indexOfFirst { s -> s.prefix == it })
        }
    }

    fun update() {
        val excelFile = File(settings.excelFilePath)
        val saveFile = File(settings.saveFilePath)
        saveSlides(excelFile, saveFile, settings)
        slides = ArrayList(loadSlides(saveFile).second)
        loadIndex(currIndex)
    }

    fun nextPage() {
        loadIndex(currIndex + 1)
    }

    fun prevPage() {
        loadIndex(currIndex - 1)
    }

    fun goBack() {
        replaceWith<MainOptions>()
    }
}